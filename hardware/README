This directory is part of FPC-III, the Freely Programmable Computer.

The following notice applies to the FPC-III design, recorded in the
files *.sch and fpc-iii.kicad_pcb:

Copyright 2020, Gary Wong <gtw@gnu.org>

FPC-III is a free hardware design: you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

FPC-III is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with FPC-III.  If not, see <https://www.gnu.org/licenses/>.

------------

The FPC-III design also incorporates third-party library components
(schematic symbols, PCB footprints, and 3D models; found in the
*.lib, fpc-iii.pretty/*, and fpc-iii.3dmodels/* files respectively),
licensed under Creative Commons CC-BY-SA 4.0 (see COPYING.kicad
for the terms for those files).
