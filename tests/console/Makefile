# Point to a suitable monospaced TTF font: DejaVu Sans Mono will work, see:
#     https://dejavu-fonts.github.io/
FONT=$(shell fc-match -f %{file} mono)

all: console.svf

console.json: top.v usbkbd.v hdmi.v chargen.v font.hex char0.hex attr0.hex char1.hex attr1.hex
	yosys -e . -q -p "synth_ecp5 -json $@" top.v usbkbd.v hdmi.v chargen.v

font.hex: font $(FONT)
	./font $(FONT) > $@

font: font.c
	$(CC) -o $@ $< -I/usr/include/freetype2 -lfreetype

char0.hex attr0.hex char1.hex attr1.hex: char
	./char

char: char.c

%.config: %.json fpc-iii.lpf
	nextpnr-ecp5 --json $< --textcfg $@ --85k --speed 8 --package CABGA381 --lpf fpc-iii.lpf

%.svf: %.config
	ecppack --compress --svf $@ $<

%-flash.svf: %.config
	ecppack --compress --svf-spiflash --freq 62.0 --spimode dual-spi --svf $@ $<

install: console.svf
	openocd -l openocd.log -f fpc-iii.cfg -c "init; svf $<; exit"

flash: console-flash.svf
	openocd -l openocd.log -f fpc-iii.cfg -c "init; svf $<; exit"

clean:
	rm -f *.svf *.config *.json *.log

.PHONY: all install clean
